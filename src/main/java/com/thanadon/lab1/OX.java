/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.lab1;

import java.util.Scanner;

/**
 *
 * @author Acer
 */
public class OX {
    
    public static void setBoard(char[][] arr){
        for(int i = 0; i < 3; i++)
                    for(int j = 0; j < 3; j++)
                        arr[i][j] = '-';
    }
    
    public static void printBoard(char[][] arr){
        for(int i = 0; i < 3; i++){
                    for(int j = 0; j < 3; j++)
                         System.out.print(arr[i][j]+"  ");
                    System.out.println();
        }
    }
    
    public static boolean checkRow(char[][] arr, int i, char symbol){
        if(arr[i][0] == symbol & arr[i][1] == symbol & arr[i][2] == symbol)
            return true;
        
        else
            return false;
    }  
    
    public static boolean checkColumn(char[][] arr, int j, char symbol){
        if(arr[0][j] == symbol & arr[1][j] == symbol & arr[2][j] == symbol)
            return true;
        
        else
            return false;
    }
    
    public static boolean checkDiagonal(char[][] arr, char symbol){
        if(arr[0][0] == symbol & arr[1][1] == symbol & arr[2][2] == symbol)
            return true;
        
        else if(arr[0][2] == symbol & arr[1][1] == symbol & arr[2][0] == symbol)
            return true;
        
        else
            return false;
    }
    
    public static boolean checkStatus(char[][] arr, char symbol){
        for(int i = 0; i < 3; i++)
            if(checkRow(arr, i, symbol))  
                return true;
        
        for(int j = 0; j < 3; j++)
            if(checkColumn(arr, j, symbol)) 
                return true;
        
        if(checkDiagonal(arr, symbol))
            return true;
        
        return false;
    }
    
    public static char whoTurn(int round){
        if(round%2 == 1){
                  return 'O';
              }   
              else{
                  return 'X';
              }
    }
    
    public static void main(String[] args) {
        
        Scanner kb = new Scanner(System.in);
        char[][] board = new char[3][3]; 
        int round = 1;
        char symbol = ' ';
        boolean draw = true;
                
        System.out.println("Welcome to OX Game");
        setBoard(board);
        printBoard(board);
        
        while(round <= 9){
     
              symbol = whoTurn(round);
              
              System.out.println("Turn "+symbol);
              
              System.out.println("Please input row, col: ");
              int i = kb.nextInt();
              int j = kb.nextInt();
              
              board[i-1][j-1] = symbol;

              System.out.println();
              printBoard(board);
              if(checkStatus(board, symbol)){
                  System.out.println(">>>"+symbol+" Win"+"<<<");
                  draw = false;
                  break;
              }
              round++;
         }
        
        if(draw)
            System.out.println(">>>Draw<<<");
                    
    }
            
}
